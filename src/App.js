import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import React, { useState } from 'react';


function App() {

  const [imageUrl, setImageUrl] = useState(null);
  const [imageData, setImageData] = useState(null);
  const [clickUrl, setClickUrl] = useState(null);
  const [clickDestination, setClickDestinationUrl] = useState(null);
  const [viewabilityUrl, setViewabilityUrl] = useState(null);



  const randomValue = Math.round(Math.random() * 100000000)

  const apiURL = `https://tportdev-ads.aimatch.com/tportdev/bserverj/ball/site=Inspire-Me/POSSTATEPROV=FL/ADSIZE=300x250/vid=d86a78c6-673c-4031-8b69-2bdc95ca9784/viewid="${ randomValue }"/random="${ randomValue }"/b1/position=1/b2/position=2`;

  const fetchAdsDirect = async () => {
    const response = await axios.get(apiURL)

    const firstTag = "<ImageURL>";
    const secondTag = "</ImageURL>";

    console.log(response.data[0]);

    const matches = response.data[0].match(new RegExp(firstTag + "(.*)" + secondTag));

    setImageUrl(matches[1]) 
  }

  const fetchNewAds = async () => {
    const internalServiceURL = `https://stag-dis.platform.mttnow.com/dis/graphical?destinationCity=jfk&numberOfAds=2&site=Smartpoint-Plus`;

    const response = await axios.get(internalServiceURL)

    if (response.status == 200) {
      console.log(response.data.ads[0]);

      setImageData(response.data.ads[0].imageData)
      setClickUrl(response.data.ads[0].click.trackingUrl)
      setClickDestinationUrl(response.data.ads[0].click.destinationUrl)
      setViewabilityUrl(response.data.ads[0].viewability.url)
    }

  }

  const imageClick = async (clickUrl) => {


    axios.get("https://stag-dis.platform.mttnow.com/dis/graphical/event", { headers: { location: viewabilityUrl } })
    .then(response => {
     })
    .catch((error) => {
        console.log('error ' + error);
     });   
     
     axios.get("https://stag-dis.platform.mttnow.com/dis/graphical/event", { headers: { location: clickUrl } })
    .then(response => {
      window.open(clickDestination);
     })
    .catch((error) => {
        console.log('error ' + error);
     });    
  } 

  return (
    <div className="App">
      <div>
        <button className="fetch-ads-direct" onClick={fetchAdsDirect}>
          Retrieve Ad directly from SAS
        </button>

        {imageUrl && imageUrl.length > 0 &&
        <h2>
          <img 
            src={ imageUrl }
            alt="new"
          />
        </h2>
        }

        <button className="fetch-ads-via-service" onClick={fetchNewAds}>
          Retrieve Ads via internal service
        </button>

        {imageData && imageData.length > 0 &&
        <h2>
          <img 
            src={`data:image/png;base64,${imageData}`}
            alt="new"
            onClick={() => imageClick(clickUrl)}
          />
        </h2>
        }
      </div>
    </div>
  );
}

export default App;
